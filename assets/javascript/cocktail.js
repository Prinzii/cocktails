

function getCocktail (id){
  var xhttp = new XMLHttpRequest();
  if (id) {
    xhttp.open("GET", `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`, true);
  } else {
    xhttp.open("GET", `https://www.thecocktaildb.com/api/json/v1/1/random.php`, true);
  }
  xhttp.onload = function(e) {
    var data = JSON.parse( xhttp.response )
    fillCocktailData(data.drinks[0])
  };
  xhttp.send();
}

function getParameterByName(name, url = window.location.href) {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function fillCocktailData(cocktailData) {
  const regexp = /^strMeasure([0-9]+)$/;
  const ingredientsList = document.getElementById("ingredients-list")
  for ( const dataKey in cocktailData ) {
    if ( dataKey.match(regexp) ) {
      const measureStr = cocktailData[dataKey]
      const ingredientStr = cocktailData[`strIngredient${dataKey.match(regexp)[1]}`]
      if ( !measureStr ) {
        continue
      }
      var li = document.createElement("li");

      var measure = document.createElement("span");
      measure.innerHTML = measureStr  + " ";
      measure.classList.add("measure");
      li.appendChild(measure);

      var ingredient = document.createElement("span");
      ingredient.innerHTML = ingredientStr;
      ingredient.classList.add("ingredient");
      li.appendChild(ingredient);

      ingredientsList.appendChild(li);
    }
  }
  const img = document.getElementById("preview-image")
  img.src = cocktailData.strDrinkThumb

  const instructions = document.getElementById("instructions")
  instructions.innerHTML = cocktailData.strInstructions

  const cocktailName = document.getElementById("cocktail-name")
  cocktailName.innerHTML = cocktailData.strDrink
}

var cocktailID = getParameterByName("cocktail")
getCocktail(cocktailID)
