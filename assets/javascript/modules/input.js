export class Input {
  input
  wrapper
  error
  valid = false

  constructor( name, label, type="text" ){
    const inputId = `${name}_input`
    this.wrapper = document.getElementById(name);
    if (!this.wrapper ) {
      return
    }
    this.wrapper.classList.add("material-form-field")

    this.input = document.createElement("input");
    this.input.id = inputId
    this.input.required = true;
    this.input.type = type
    this.input.onkeyup = this.onLocalChange

    this.error = document.createElement("div");
    this.error.classList.add("error-string");

    var labelElement = document.createElement("label");
    labelElement.htmlFor = inputId
    labelElement.innerHTML = label


    this.wrapper.appendChild(this.input)
    this.wrapper.appendChild(labelElement)
    this.wrapper.appendChild(this.error)
  }

  onLocalChange = () => {
    const input = this.input.value
    let error = this.errorFunction(input);
    this.wrapper.classList.remove("error")
      this.error.innerHTML = ""
    if (!!error && error !== "") {
      this.setError(error)
      this.valid = false
      this.wrapper.classList.add("error")
    } else {
      this.valid = true
    }
    this.afterChange(input)
  }

  afterChange = () => { }

  setError = (error) => {
    this.error.innerHTML = error
  }

  errorFunction = (value) => {
    return ""
  }
}
