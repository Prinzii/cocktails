import { Navigator } from './navigator.js';
var navigator = new Navigator( "search-cocktail" );


export function getParameterByName(name, url = window.location.href) {
  name = name.replace(/[\[\]]/g, '\\$&')
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url)
  if (!results) {
    return null
  }
  if (!results[2]){
     return ''
   }
  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

export function renderFavorites(){
  var currentFavorites = JSON.parse(localStorage.getItem('favorites') || '{}');
  const favorits = document.getElementById("favorits")
  if (!favorits ) {
    return
  }
  favorits.innerHTML = ""
  for ( let id in currentFavorites ) {
    if (!currentFavorites[id]){
      continue
    }
    let currentID = id
    var wrapper = document.createElement("div");
    wrapper.classList.add("favorite-wrapper")
    var newFavorite = document.createElement("a");
    newFavorite.innerHTML = currentFavorites[id].name
    newFavorite.classList.add("favoriten")
    newFavorite.href = `cocktail.html?cocktail=${currentID}`
    var clear = document.createElement("i");
    clear.classList.add("material-icons");
    clear.innerHTML = "clear"
    clear.onclick = function() {

      var currentFavorites = JSON.parse(localStorage.getItem('favorites') || '{}');
      const star = document.getElementById(`star_${currentID}`)
      if ( star ) {
        star.innerHTML = "star_border"
        star.classList.remove("favorite");
      }
      currentFavorites[currentID] = null
      localStorage.setItem('favorites', JSON.stringify(currentFavorites))
      renderFavorites()
    }
    wrapper.appendChild(newFavorite)
    wrapper.appendChild(clear)
    favorits.appendChild(wrapper)
  }
}

renderFavorites()
