export class Navigator {
  wrapper

  constructor( ){
    this.wrapper = document.getElementById("navigator");
    this.wrapper.classList.add("navigator");

    const header = document.createElement("h1");
    header.innerHTML = "Navigation"
    this.wrapper.appendChild(header)

    const elements = [
      {label: "Cocktail Search", icon: "search", link: "index.html"},
      {label: "Random Cocktail", icon: "shuffle", link: "cocktail.html"},
      {label: "Register", icon: "person", link: "register.html"},
      {label: "Show me a chart", icon: "show_chart", link: "chart.html"},
    ]
    this.renderElements(elements, this.wrapper.dataset.current)
  }

  renderElements= (elements, current) => {
    for (let index in elements ) {
      const element = elements[index]
      const link = document.createElement("a");
      if ( element.label === current ) {
        link.classList.add("current");
      }
      link.classList.add("navEntryWrapper");
      link.href = element.link

      const icon = document.createElement("i");
      icon.classList.add("material-icons");
      icon.innerHTML = element.icon
      link.appendChild(icon)

      const text = document.createElement("span");
      text.classList.add("navEntry")
      text.innerHTML = element.label
      link.appendChild(text)
      this.wrapper.appendChild(link)
    }
  }
}
