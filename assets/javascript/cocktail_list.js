import { Input } from './modules/input.js';
import { renderFavorites } from './modules/main.js';

const searchCocktails = (str) => {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${str}`, true);
  xhttp.onload = function(e) {
    let data = JSON.parse( xhttp.response )
    data = data.drinks;
    let list = document.getElementById("cocktail-list")
    list.innerHTML = "";
    for ( let cocktail in  data) {
      list.appendChild(addCocktail(data[cocktail]));
    }
  };
  xhttp.onerror = function(e) {
    alert("Sorry, did not work. Try again later")
  };
  xhttp.send();
}

const addCocktail = (cocktailData)=> {
  let currentFavorites = JSON.parse(localStorage.getItem('favorites')||'{}');
  var cocktail = document.createElement("div");
  cocktail.classList.add("cocktail");

  var link = document.createElement("a");
  link.href = `cocktail.html?cocktail=${cocktailData.idDrink}`

  var img = document.createElement("img");
  img.src = cocktailData.strDrinkThumb
  img.alt = `Image of ${cocktailData.strDrink}`
  link.appendChild(img)
  cocktail.appendChild(link)

  var header = document.createElement("h1");
  header.innerHTML = cocktailData.strDrink;
  cocktail.appendChild(header);
  var subheader = document.createElement("h2");
  subheader.innerHTML = cocktailData.strCategory;
  cocktail.appendChild(subheader);

  let isFavorite = !!currentFavorites[cocktailData.idDrink];

  var star = document.createElement("i");
  star.classList.add("material-icons");
  star.classList.add("star");
  if ( isFavorite ) {
    star.classList.add("favorite");
    star.innerHTML = "star";
  } else {
    star.innerHTML = "star_border";
  }
  star.id = `star_${cocktailData.idDrink}`
  star.onclick = function() {
    currentFavorites = JSON.parse(localStorage.getItem('favorites') || '{}');
    isFavorite = !!currentFavorites[cocktailData.idDrink];
    if (!isFavorite) {
      star.innerHTML = "star"
      star.classList.add("favorite");
      currentFavorites[cocktailData.idDrink] = {
        id: cocktailData.idDrink,
        name: cocktailData.strDrink,
        type: cocktailData.strCategory
      }
    } else {
      star.innerHTML = "star_border"
      star.classList.remove("favorite");
      delete currentFavorites[cocktailData.idDrink]
    }
    localStorage.setItem('favorites', JSON.stringify(currentFavorites))
    renderFavorites()
  };
  cocktail.appendChild(star);


  return cocktail
}

setTimeout(()=>{
  renderFavorites()
  const input = document.getElementById("search-cocktail_input")
  let value = input.value
  searchCocktails(value)
},1)

var searchCocktailsInput = new Input( "search-cocktail", "Search" );
searchCocktailsInput.afterChange = searchCocktails
