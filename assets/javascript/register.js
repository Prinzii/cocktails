import { Input } from './modules/input.js';

var firstName = new Input( "first_name", "First Name" );
var lastName = new Input( "last_name", "Last Name" );
var email = new Input( "email", "Email", "email" );
var password = new Input( "password", "Password", "password" );

function nameVaild(name) {
  if (name.length === 0 ) {
    return "Must not be empty"
  }
  if ( name.includes( " " ) ) {
    return "Enter only one name"
  }
}

function emailValid(email) {
  if (!email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/)) {
    return "Please enter a valid email"
  }
}

function passwordVaild(pw) {
  if (pw.length < 8 ) {
    return "Password has to have more than 8 characters"
  }
  if (!pw.match(/\d/)) {
    return "Password has to include a number"
  }
  if (!pw.match(/[A-Z]/)) {
    return "Password has to include uppercase letter"
  }
  if (!pw.match(/[a-z]/)) {
    return "Password has to include lowercase letter"
  }
}


function myFunction() {
  alert(`Nice to meet you ${firstName.input.value}`)
}

function validateButton(){
  const button = document.getElementById("register-button")
  button.onclick = myFunction
  if ( firstName.valid && lastName.valid && email.valid && password.valid ) {
    button.classList.remove("disable")
  } else {
    button.classList.add("disable")
  }
}


firstName.errorFunction = nameVaild
lastName.errorFunction = nameVaild
email.errorFunction = emailValid
password.errorFunction = passwordVaild
firstName.afterChange = validateButton
lastName.afterChange = validateButton
email.afterChange = validateButton
password.afterChange = validateButton
