var ctx = document.getElementById('myChart').getContext('2d');

var favorits = JSON.parse(localStorage.getItem('favorites') || '{}');
if (favorits ) {

  const labels = []
  const data = []
  let count = {}

  for ( const cocktailId in favorits) {
    const cocktail = favorits[cocktailId]
    if (!cocktail) {
      continue
    }
    if (!count[cocktail.type]) {
      count[cocktail.type] = 0
      labels.push(cocktail.type)
    }
    count[cocktail.type] ++
  }

  for ( const nameIndex in labels) {
    const name = labels[nameIndex]
    data.push(count[name])
  }

  debugger

  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels: labels,
      datasets: [{
        data: data,
        backgroundColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
}
